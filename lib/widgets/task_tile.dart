import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todoey/models/tasks_list_model.dart';

class TaskTile extends StatelessWidget {
  final String taskTitle;
  final bool isChecked;
  final int index;

  TaskTile({this.taskTitle, this.isChecked, this.index});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onLongPress: () {
        Provider.of<TasksListModel>(context, listen: false).deleteTask(index);
      },
      title: Text(
        taskTitle,
        style: TextStyle(
          decoration:
              isChecked ? TextDecoration.lineThrough : TextDecoration.none,
        ),
      ),
      trailing: Checkbox(
        activeColor: Colors.lightBlueAccent,
        value: isChecked,
        onChanged: (value) {
          Provider.of<TasksListModel>(context, listen: false)
              .checkOffTask(index);
        },
      ),
    );
  }
}
